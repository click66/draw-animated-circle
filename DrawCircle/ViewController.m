//
//  ViewController.m
//  DrawCircle
//
//  Created by Don Pironet on 23/03/15.
//  Copyright (c) 2015 donpironet. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (nonatomic, strong) CAShapeLayer *circle;
@property (nonatomic) int radius;

// Needs to come from the backend in an object or something.
@property (nonatomic, strong) NSArray *votes;
@property float totalListens;


@property (nonatomic, strong) NSArray *segmentsStart;
@property (nonatomic, strong) NSArray *segmentsEnd;
@property (nonatomic, strong) NSArray *percentages;

@property (nonatomic, strong) NSArray *colors;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _colors = @[[UIColor redColor], [UIColor greenColor], [UIColor blueColor], [UIColor blackColor]];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self _initCircle];
    [self _startProcessing];
    
}

- (void)_startProcessing{
    // These things need to come from the backend. It's just for testing purposes.
    _totalListens = 200;
    _votes = @[@50.0f, @50.0f, @50.0f, @50.0f];
    
    // TODO write function to calculate these values
    _segmentsStart = @[@0.0f, @0.25f, @0.50f, @0.75f];
    _segmentsEnd = @[@0.25f, @0.50f, @0.75f, @1.0f];
    
    [self _drawStrokeOnCircle];
    
}

- (void)_drawStrokeOnCircle {
    
    [self _initCircle];
    
    [CATransaction begin];
    
    for (NSUInteger i = 0; i < _votes.count; i++) {
        CAShapeLayer* strokePart = [[CAShapeLayer alloc] init];
        strokePart.fillColor = [[UIColor clearColor] CGColor];
        strokePart.frame = _circle.bounds;
        strokePart.path = _circle.path;
        strokePart.lineCap = _circle.lineCap;
        strokePart.lineWidth = _circle.lineWidth;
        
        
        // Config appearance off the circle
        strokePart.strokeColor = ((UIColor *)_colors[i]).CGColor;
        strokePart.strokeStart = [_segmentsStart[i] floatValue];
        strokePart.strokeEnd = [_segmentsEnd[i] floatValue];
        
        [_circle addSublayer: strokePart];
        
        // Configure animation
        CAKeyframeAnimation* drawAnimation = [CAKeyframeAnimation animationWithKeyPath: @"strokeEnd"];
        drawAnimation.duration = 1.0f;
        
        // Each value in the array is a floating point number between 0.0 and 1.0 and corresponds to one element in the values array. Each element in the keyTimes array defines the duration of the corresponding keyframe value as a fraction of the total duration of the animation. Each element value must be greater than, or equal to, the previous value.
        NSArray* times = @[ @(0.0),
                            @(strokePart.strokeStart),
                            @(strokePart.strokeEnd),
                            @(1.0) ];
        
        // The keyframe values represent the values through which the animation must proceed. The time at which a given keyframe value is applied to the layer depends on the animation timing, which is controlled by the calculationMode, keyTimes, and timingFunctions properties. Values between keyframes are created using interpolation, unless the calculation mode is set to kCAAnimationDiscrete.
        NSArray* values = @[ @(strokePart.strokeStart),
                             @(strokePart.strokeStart),
                             @(strokePart.strokeEnd),
                             @(strokePart.strokeEnd) ];
        
        drawAnimation.keyTimes = times;
        drawAnimation.values = values;
        
        // The receiver does not appear until it begins but remains visible in its final state when it is completed.
        drawAnimation.fillMode = kCAFillModeForwards;
        
        // https://developer.apple.com/library/mac/documentation/Cocoa/Reference/CAMediaTimingFunction_class/index.html#//apple_ref/doc/constant_group/Predefined_Timing_Functions
        drawAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
        
        
        [strokePart addAnimation: drawAnimation forKey: @"drawCircleAnimation"];
    }
    
    [CATransaction commit];
}

- (void)_initCircle {
    [_circle removeFromSuperlayer];
    _circle = [CAShapeLayer layer];
    _radius = 100;
    
    // Create a circle
    _circle.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, 2.0 * _radius, 2.0 * _radius) cornerRadius:_radius].CGPath;
    
    // Center the shape in self.view
    _circle.position = CGPointMake(CGRectGetMidX(self.view.frame) - _radius,
                                   CGRectGetMidY(self.view.frame) - _radius);
    
    _circle.fillColor = [UIColor clearColor].CGColor;
    _circle.lineWidth = 5;
    
    // Add to parent layer
    [self.view.layer addSublayer:_circle];
}


@end
